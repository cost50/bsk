package tdd.training.bsk;

public class Frame {

	protected int firstThrow, secondThrow;
	protected int bonus;
	
	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		if(firstThrow<0 && firstThrow>10 && secondThrow<0 && secondThrow > 10)
			throw new BowlingException("Attention: a throw can knock down from 0 to 10 pins.");
		this.firstThrow = firstThrow;
		this.secondThrow = secondThrow;
		this.bonus=0;
		//calculate here bonus considering strike frame or spare frame, if no need of ordinary frame score
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		return this.firstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		return this.secondThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		this.bonus=bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return bonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		return this.firstThrow+this.secondThrow+this.getBonus();
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		return (this.firstThrow==10);
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		return (this.firstThrow + this.secondThrow==10);
	}
	
	
	
	@Override
	public boolean equals(Object frameObject) {
		Frame frame = (Frame) frameObject;
		return this.getFirstThrow()==frame.getFirstThrow() && this.getSecondThrow()==frame.getSecondThrow();
	}



}
