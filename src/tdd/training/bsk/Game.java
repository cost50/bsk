package tdd.training.bsk;

import java.util.ArrayList;
import java.util.List;

public class Game {

	private List<Frame> frameList;
	private int firstBonusThrow;
	private int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		this.frameList = new ArrayList<Frame>(10);
		firstBonusThrow = 0;
		secondBonusThrow = 0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		this.frameList.add(frame);
	}

	public void addFrameAt(int index, Frame frame) {
		this.frameList.add(index, frame);
	}

	public int indexOfFrame(Frame frame) {
		return this.frameList.indexOf(frame);
	}

	public void setFrame(int index, Frame frame) {
		this.frameList.set(index, frame);
	}

	public Frame getFrame(int index) throws BowlingException {
		if ( index<0 || index>9 )
			throw new BowlingException("Attention: a game is made up of 0 to 10 frames.");
		return this.frameList.get(index);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if ( index<0 || index>9 )
			throw new BowlingException("Attention: a game is made up of 0 to 10 frames.");
		return this.frameList.get(index);
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		if(firstBonusThrow<0 && firstBonusThrow>10)
			throw new BowlingException("Attention: a throw can knock down from 0 to 10 pins.");
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		if(secondBonusThrow<0 && secondBonusThrow > 10)
			throw new BowlingException("Attention: a throw can knock down from 0 to 10 pins.");
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		
		if (this.frameList.isEmpty())
			return 0;

		int totalScore = 0;
		Frame subsequentFrame; //-
		Frame currentFrame;

		for (int index = 0; index < 9; index++) {

			currentFrame = this.getFrame(index);
			
			// before isStrike(), because Strike is also a Spare
			if (currentFrame.isStrike()) { //Strike
				subsequentFrame = this.getFrame(index + 1);
				if (subsequentFrame.isStrike())
					if(index != 8)
					currentFrame.setBonus(subsequentFrame.firstThrow + this.getFrame(index + 2).firstThrow);
					else currentFrame.setBonus(subsequentFrame.firstThrow + this.getFirstBonusThrow());
				else
					currentFrame.setBonus(subsequentFrame.firstThrow + subsequentFrame.secondThrow);
			}
			else if (currentFrame.isSpare()) { //Spare
				subsequentFrame = this.frameList.get(index + 1);
				currentFrame.setBonus(subsequentFrame.firstThrow);
			}

			totalScore += currentFrame.getScore();
		}
		
		//Frame 10 (need to check spare and strike? Does first and second bonus throw exist the same?
		currentFrame = this.getFrame(9);
		
		totalScore += currentFrame.getScore();
		
		if(currentFrame.isStrike())
			totalScore += this.getFirstBonusThrow() + this.getSecondBonusThrow();
		else if(currentFrame.isSpare())
			totalScore += this.getFirstBonusThrow();
		
		return totalScore;
		
		
	}

}
