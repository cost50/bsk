package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class FrameTest {

	private int firstThrow, secondThrow;
	private Frame testFrame;
	
	@Before
	public void setUp() throws BowlingException {
		testFrame = new Frame(firstThrow, secondThrow);
		firstThrow = 2;
		secondThrow = 6;
	}
	
	@Test
	public void testGetThrow() throws BowlingException{
		//User Story 1
		assertEquals(firstThrow, testFrame.getFirstThrow());
		assertEquals(secondThrow, testFrame.getSecondThrow());
	}
	
	@Test
	public void testGetScore() throws BowlingException{
		//User Story 2
		assertEquals(firstThrow+secondThrow, testFrame.getScore());
	}
}
