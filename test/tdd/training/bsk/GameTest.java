package tdd.training.bsk;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class GameTest {

	private Game testGame;

	@Before
	public void setUp() throws BowlingException {
		// Testing Game.game()
		testGame = new Game();
		// Testing Game.addFrame(Frame frame)
		testGame.addFrame(new Frame(1, 5));
		testGame.addFrame(new Frame(3, 6));
		testGame.addFrame(new Frame(7, 2));
		testGame.addFrame(new Frame(3, 6));
		testGame.addFrame(new Frame(4, 4));
		testGame.addFrame(new Frame(5, 3));
		testGame.addFrame(new Frame(3, 3));
		testGame.addFrame(new Frame(4, 5));
		testGame.addFrame(new Frame(8, 1));
		testGame.addFrame(new Frame(2, 6));
	}

	@Test
	public void testGameInitializzation() throws BowlingException {
		// User Story 3
		List<Frame> frameList = new ArrayList<Frame>();
		frameList.add(new Frame(1, 5));
		frameList.add(new Frame(3, 6));
		frameList.add(new Frame(7, 2));
		frameList.add(new Frame(3, 6));
		frameList.add(new Frame(4, 4));
		frameList.add(new Frame(5, 3));
		frameList.add(new Frame(3, 3));
		frameList.add(new Frame(4, 5));
		frameList.add(new Frame(8, 1));
		frameList.add(new Frame(2, 6));

		// Testing Game.getFrameAt(int index)
		for (int i = 0; i < 10; i++)
			assertEquals(frameList.get(i), testGame.getFrameAt(i));
	}

	@Test
	public void testGameCalculateScore() throws BowlingException {
		// User Story 4
		int totalScore = 81;
		// Testing Game.calculateScore()
		assertEquals(totalScore, testGame.calculateScore());

		// User Story 5
		totalScore = 88;
		testGame.setFrame(0, new Frame(1, 9));
		// Testing Game.calculateScore()
		assertEquals(totalScore, testGame.calculateScore());

		// User Story 6
		// int strikeFrameScore=19;
		totalScore = 94;
		testGame.setFrame(0, new Frame(10, 0));
		// Testing Frame.getScore() if no need of ordinary frame score
		// assertEquals(strikeFrameScore, testGame.frameList.get(0).getScore());
		// Testing Game.calculateScore()
		assertEquals(totalScore, testGame.calculateScore());

		// User Story 7
		totalScore = 103;
		testGame.setFrame(1, new Frame(4, 6));
		// Testing Game.calculateScore()
		assertEquals(totalScore, testGame.calculateScore());

		// User Story 8
		totalScore = 112;
		testGame.setFrame(1, new Frame(10, 0));
		// Testing Game.calculateScore()
		assertEquals(totalScore, testGame.calculateScore());

		// User Story 9
		totalScore = 98;
		testGame.setFrame(0, new Frame(8, 2));
		testGame.setFrame(1, new Frame(5, 5));
		// Testing Game.calculateScore()
		assertEquals(totalScore, testGame.calculateScore());

	}

	@Test
	public void testBonusThrowManaging() throws BowlingException {
		// User Story 10
		int totalScore = 90;
		testGame.setFrame(9, new Frame(2, 8));
		testGame.setFirstBonusThrow(7);
		// Testing Game.calculateScore()
		assertEquals(totalScore, testGame.calculateScore());

		// User Story 11
		totalScore = 92;
		testGame.setFrame(9, new Frame(10, 0));
		testGame.setFirstBonusThrow(7);
		testGame.setSecondBonusThrow(2);
		// Testing Game.calculateScore()
		assertEquals(totalScore, testGame.calculateScore());
		
		//User Story 12
		totalScore = 300;
		for (int i=0;i<9;i++) {
			testGame.setFrame(i, new Frame(10, 0));
		}
		testGame.setFirstBonusThrow(10);
		testGame.setSecondBonusThrow(10);
		// Testing Game.calculateScore()
		assertEquals(totalScore, testGame.calculateScore());
	}
	
	

}
